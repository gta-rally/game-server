#FROM alpine:3.2
FROM sandrokeil/typescript

MAINTAINER fin.kingma@praegus.nl

RUN npm install -g typescript

#installing Exploratory Testing Game in container
RUN mkdir -p /usr/src/game-server
WORKDIR /usr/src/game-server
COPY . /usr/src/game-server
RUN npm install

EXPOSE 3000

CMD [ "npm", "run", "start" ]