import * as express from "express";
import * as socketio from "socket.io";
import * as path from "path";

const app = express();
app.set("port", process.env.PORT || 3000);

let http = require("http").Server(app);
let io = require("socket.io")(http);
let players: { [key: string]: any } = {};
let highscore:number = 0
let highscoreHolder:string = ''

io.on("connection", (socket: any) => {
    console.log("a user connected");
    
    socket.on('new-player', (player:any) => {
        console.log("new player found: " + player.name);
        socket.broadcast.emit('new-player', player)
        io.to(`${socket.id}`).emit('initial-players', players)
        io.to(`${socket.id}`).emit('highscore', highscore, highscoreHolder)
        players[socket.id] = player
    });

    socket.on('bumped-user', (bumpData:any) => {
      console.log('bump detected: ' + JSON.stringify(bumpData, null, 2))
      io.to(`${bumpData.id}`).emit('bounce-back', {
        speed: bumpData.speed, 
        direction: bumpData.direction,
        damage: bumpData.damage,
        playerId: bumpData.playerId
      })
    });

    socket.on('send-frag', (bumpData:any) => {
      console.log('player ' + bumpData.playerId + ' gained a frag')
      io.to(`${bumpData.playerId}`).emit('receive-frag')
    });

    socket.on('disconnect', () => {
        console.log("player disconnected");
        delete players[socket.id]
        io.emit('remove-player', socket.id)
      })

    socket.on('update-player-event', (player: any) => {
      if (players[player.id] != undefined) {
        players[player.id].topPos = player.topPos
        players[player.id].leftPos = player.leftPos
        players[player.id].speed = player.speed
        players[player.id].moveDirection = player.moveDirection
        players[player.id].carDirection = player.carDirection
        players[player.id].traction = player.traction
        players[player.id].health = player.health
        players[player.id].killstreak = player.killstreak
        players[player.id].activeWeapon = player.activeWeapon
        players[player.id].phasing = player.phasing

        console.log('updating player with: ' + player.event)
        socket.broadcast.emit('update-player-event', player);

        if (player.killstreak > highscore) {
          highscore = players[player.id].killstreak
          highscoreHolder = players[player.id].name
          io.emit('highscore', highscore, highscoreHolder)
        }
      } else {
        console.log('could not update player ' + player.id + '. Player does not exist yet.')
      }
    });
});

let toRadians = (angle:number) => {
  return angle * (Math.PI / 180)
}

const server = http.listen(3000, function() {
  console.log("listening on *:3000");
});